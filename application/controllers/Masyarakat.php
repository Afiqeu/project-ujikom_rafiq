<?php

use LDAP\Result;

defined('BASEPATH') or exit('No direct script access allowed');

class Masyarakat extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('auth_model');
		$this->load->model('profile_model');
		$this->load->model('Pengaduan_m');
	}

	public function index()
	{

		$data['masyarakat'] = $this->db->get_where('masyarakat', ['username' =>
		$this->session->userdata('username')])->row_array();

		$this->load->view('user/index', $data);
	}

	public function profile()
	{
		$data['title'] = 'Profile';

		$data['masyarakat'] = $this->db->get_where('masyarakat', ['username' =>
		$this->session->userdata('username')])->row_array();

		$this->load->view('templates/auth_header');
		$this->load->view('templates/auth_footer');
		$this->load->view('user/profile_utama', $data);
	}
	public function change_image()
	{
		$data['title'] = 'Ganti Poto';

		$data['masyarakat'] = $this->db->get_where('masyarakat', ['username' =>
		$this->session->userdata('username')])->row_array();

		$this->load->view('templates/auth_header');
		$this->load->view('templates/auth_footer');
		$this->load->view('user/profile', $data);
	}
	public function upload_image()
	{
		$this->load->model('profile_model');
		$this->load->model('auth_model');
		$data['current_user'] = $this->auth_model->current_user();

		$file_name = str_replace('.', '', $data['current_user']->nik);
		$config['upload_path']          = realpath(APPPATH . '../assets/img/profile/');
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['file_name']            = $file_name;
		$config['overwrite']            = true;
		$config['max_size']             = 1024;  //1MB
		$config['max_width']            = 1024;
		$config['max_height']           = 1024;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('avatar')) {
			$data['error'] = $this->upload->display_errors();
			var_dump($data['error']);
		} else {
			$uploaded_data = $this->upload->data();

			$new_data = [
				'nik' => $data['current_user']->nik,
				'image' => $uploaded_data['file_name'],
			];

			if ($this->profile_model->update($new_data)) {
				$this->session->set_flashdata('message', 'Avatar was updated');
				redirect(site_url('masyarakat/profile'));
			}
		}
	}
	public function ganti_password()
	{
		$data['title'] = 'Ganti Password';
		$this->load->library('form_validation');
		$this->load->model('profile_model');
		$data['current_user'] = $this->auth_model->current_user();

		if ($this->input->method() === 'post') {
			$rules = $this->profile_model->password_rules();
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() === FALSE) {
				return $this->load->view('user/ganti_password.php', $data);
			}

			$new_password_data = [
				'nik' => $data['current_user']->nik,
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			];

			if ($this->profile_model->update($new_password_data)) {
				$this->session->set_flashdata('message', 'Password was changed');
				redirect(site_url('masyarakat/ganti_password'));
			}
		}

		$this->load->view('user/ganti_password.php', $data);
	}
	public function remove_image()
	{
		$current_user = $this->auth_model->current_user();
		$this->load->model('auth_model');

		// hapus file
		$file_name = str_replace('.', '', $current_user->nik);

		// set avatar menjadi null
		$new_data = [
			'nik' => $current_user->nik,
			'image' => 'user.jpg',
		];

		if ($this->profile_model->update($new_data)) {
			$this->session->set_flashdata('message', 'Avatar dihapus!');
			redirect(site_url('masyarakat/profile'));
		}
	}
	public function pengaduan()
	{
		$data['title'] = 'Pengaduan';
		$masyarakat = $this->db->get_where('masyarakat', ['username' => $this->session->userdata('username')])->row_array();
		$data['data_pengaduan'] = $this->Pengaduan_m->data_pengaduan_masyarakat_nik($masyarakat['nik'])->result_array();

		$this->form_validation->set_rules('isi_laporan', 'Isi Laporan Pengaduan', 'trim|required');
		$this->form_validation->set_rules('foto', 'Foto Pengaduan', 'trim');

		if ($this->form_validation->run() == FALSE) :
			$this->load->view('templates/auth_header');
			$this->load->view('templates/auth_footer');
			$this->load->view('user/pengaduan', $data);

		else :
			$upload_foto = $this->upload_foto('foto'); // parameter nama foto
			if ($upload_foto == FALSE) :
				$this->session->set_flashdata('message', '
					Upload foto pengaduan gagal, hanya png,jpg dan jpeg yang dapat di upload!');
				redirect('Masyarakat/pengaduan');
			else :

				$params = [
					'tgl_pengaduan'  	=> date('Y-m-d'),
					'nik'				=> $masyarakat['nik'],
					'isi_laporan'		=> htmlspecialchars($this->input->post('isi_laporan', true)),
					'foto'				=> $upload_foto,
					'status'			=> '0',
				];

				$resp = $this->Pengaduan_m->create($params);

				if ($resp) :
					$this->session->set_flashdata('message', '
						Laporan berhasil dibuat');

					redirect('Masyarakat/pengaduan');
				else :
					$this->session->set_flashdata('message', '
						Laporan gagal dibuat!');

					redirect('Masyarakat/pengaduan');
				endif;

			endif;
		endif;
	}
	public function pengaduan_baru()
	{
		$data['title'] = 'Tulis Pengaduan';
		$masyarakat = $this->db->get_where('masyarakat', ['username' => $this->session->userdata('username')])->row_array();
		$data['data_pengaduan'] = $this->Pengaduan_m->data_pengaduan_masyarakat_nik($masyarakat['nik'])->result_array();

		$this->form_validation->set_rules('isi_laporan', 'Isi Laporan Pengaduan', 'trim|required');
		$this->form_validation->set_rules('foto', 'Foto Pengaduan', 'trim');

		if ($this->form_validation->run() == FALSE) :
			$this->load->view('templates/auth_header');
			$this->load->view('templates/auth_footer');
			$this->load->view('user/pengaduan_baru', $data);

		else :
			$upload_foto = $this->upload_foto('foto'); // parameter nama foto
			if ($upload_foto == FALSE) :
				$this->session->set_flashdata('message', '
					Upload foto pengaduan gagal, hanya png,jpg dan jpeg yang dapat di upload!');
				redirect('Masyarakat/pengaduan_baru');
			else :

				$params = [
					'tgl_pengaduan'  	=> date('Y-m-d'),
					'nik'				=> $masyarakat['nik'],
					'isi_laporan'		=> htmlspecialchars($this->input->post('isi_laporan', true)),
					'foto'				=> $upload_foto,
					'status'			=> '0',
				];

				$resp = $this->Pengaduan_m->create($params);

				if ($resp) :
					$this->session->set_flashdata('message', '
						Laporan berhasil dibuat');

					redirect('Masyarakat/pengaduan_baru');
				else :
					$this->session->set_flashdata('message', '
						Laporan gagal dibuat!');

					redirect('Masyarakat/pengaduan_baru');
				endif;

			endif;
		endif;
	}
	public function pengaduan_detail($id)
	{

		$cek_data = $this->db->get_where('pengaduan',['id_pengaduan' => htmlspecialchars($id)])->row_array();

		if ( ! empty($cek_data)) :

			$data['title'] = 'Detail Pengaduan';
			$data['data_pengaduan'] = $this->Pengaduan_m->data_pengaduan_tanggapan(htmlspecialchars($id))->row_array();
			if ($data['data_pengaduan']) :
				$this->load->view('templates/auth_header');
				$this->load->view('templates/auth_footer');
				$this->load->view('_part/backend_head', $data);
				$this->load->view('user/pengaduan_detail');
				$this->load->view('_part/backend_footer_v');
				$this->load->view('_part/backend_foot');
			else :
				$this->session->set_flashdata('message', '
					Pengaduan sedang di proses!');

				redirect('Masyarakat/pengaduan');
			endif;

		else :
			$this->session->set_flashdata('message', '
				data tidak ada');

			redirect('Masyarakat/pengaduan');
		endif;
	}
	private function upload_foto($foto)
	{
		$config['upload_path']          = realpath(APPPATH . '../assets/uploads/');
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['max_size']             = 2048;
		$config['remove_spaces']        = TRUE;
		$config['detect_mime']        	= TRUE;
		$config['mod_mime_fix']        	= TRUE;
		$config['encrypt_name']        	= TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload($foto)) :
			return FALSE;
		else :
			return $this->upload->data('file_name');
		endif;
	}
}
