<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthLogin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Petugas_m');
    }
    
   
    public function index()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login page';
            $this->load->view('templates/auth_header');
            $this->load->view('admin/login_admin');
            $this->load->view('templates/auth_footer');
        } else {
            $this->login_admin();
        }
    }
    private function login_admin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $petugas = $this->db->get_where('petugas', ['username' => $username])->row_array();

        // jika petugasnya ada
        if ($petugas) {
            // jika akun petugas == TRUE
            // cek password
            if (password_verify($password, $petugas['password'])) {
                // jika password benar
                // maka buat session userdata
                $session = [
                    'username'         => $petugas['username'],
                    'level'            => $petugas['level'],
                ];
                $this->session->set_userdata($session);
                if ($petugas['level'] == 'admin') {
                    return redirect('Admin/DashboardController');
                } elseif ($petugas['level'] == 'petugas')
                    return redirect('Admin/DashboardController');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">password salah!</div>');
                redirect('admin/AuthLogin');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">username tidak terdaftar</div>');
            redirect('admin/AuthLogin');
        }
    }
    
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('password');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">You have been logged out!</div>');
        redirect('admin/AuthLogin');
    }
}